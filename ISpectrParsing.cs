﻿using System;

namespace SpectrBoomBass
{
	/// <summary> Интерфейс класса SpectrParsing для получения его необходимых значений. </summary>
	public interface ISpectrParsing
	{
		/// <summary> GetValue </summary>
		float GetValue { get; }

		/// <summary> Поднимается ли нота. </summary>
		bool IsJump { get; }

		/// <summary> Является наивысшей (или одной из) нотой спектра. </summary>
		bool IsHigh { get; }

		/// <summary> Статус высокой ноты:
		/// <remarks> 0 - Не высокий.
		/// <remarks> 1 - Стал высоким.
		/// <remarks> 2 - Остаётся высоким. </remarks></remarks></remarks> </summary>
		byte HeightStatus { get; set; }

		/// <summary> Get Buffer Value </summary>
		float[] GetBufferValue { get; }

		/// <summary> Buffer count </summary>
		int GetBufferLength { get; }

		int GetIndex { get; }

		/// <summary> Был высокий прыжок. </summary>
		bool WasHighJump { get; }

	}
}
