﻿using System;
using System.IO;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Fx;

/*												  |        |
 * * * * * * * * * * * * * * * * * * * * * * * *  ▐      | ▐|  |
 * ╔═╗┌─┐┌─┐┌─┐┌┬┐┬─┐╔╗ ┌─┐┌─┐┌┬┐╔╗ ┌─┐┌─┐┌─┐  * |▐      ▐ ▐▐| ▐|   |
 * ╚═╗├─┘├┤ │   │ ├┬┘╠╩╗│ ││ ││││╠╩╗├─┤└─┐└─┐  * ▐▐| |  ▐▐▐▐▐▐|▐▐| |▐
 * ╚═╝┴  └─┘└─┘ ┴ ┴└─╚═╝└─┘└─┘┴ ┴╚═╝┴ ┴└─┘└─┘  * ▐▐▐|▐|▐▐▐▐▐▐▐▐▐▐▐|▐▐
 * * * * * * * * * * * * * * * * * * * * * * * * ▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐▐
*///				Version 0.6.1				 =====================

namespace SpectrBoomBass
{
	/// <summary> Менеджер спектра и всей библиотеки в целом. </summary>
	public class SpectrManager : SpectrParsingManager
	{
		/// <summary> Версия библиотеки. </summary>
		public const string Version = "0.6.1";

		#region Prop

		/// <summary> Получить последнюю ошибку как библиотеки BASS, так и лога (lastError). </summary>
		public string GetLastError
		{
			get
			{
				if (lastError != null && lastError.Trim().Length > 0)
					return "ERROR: " + lastError + "\r\nBASS ERROR: " + Bass.BASS_ErrorGetCode();
				return "BASS ERROR: " + Bass.BASS_ErrorGetCode();
			}
		}

		/// <summary> Возвращает true, если инициализация BASS в методе **Init** была успешной. </summary>
		public bool BassReady { get; private set; }

		/// <summary> Получить обработанный спектр, использую конфигурации класса SpectrConf. Длинна массива = 256/SpectrConf.FFTInOne . </summary>
		public float[] GetFlts
		{
			get { return fltsCurrentValues; }
		}

		/// <summary> Повторять трек по завершению. </summary>
		public bool Repeat { get; set; }

		/// <summary> Возвращает true, если аудио была успешно загружена. </summary>
		public bool SongReady
		{
			get { return songStreamOriginal != 0 && songStreamFx != 0 && songStreamOffset != 0; }
		}

		/// <summary> Получить прогресс пройденного времени трека в формате - "00:00 - 00:00" (прошло - всего).
		/// <remarks> Getting the elapsed and remaining time. </remarks> </summary>
		public string GetSongTime
		{
			get
			{
				if (!SongReady)
					return "00:00 - 00:00";

				// сколько секунд прошло от начала воспроизведения
				timePos =
					TimeSpan.FromSeconds(Bass.BASS_ChannelBytes2Seconds(songStreamOffset,
						Bass.BASS_ChannelGetPosition(songStreamOffset, 0)));

				return timePos.Minutes + ":" + timePos.Seconds + " - " + timeLen.Minutes + ":" + timeLen.Seconds;
			}
		}

		/// <summary> Возвращает true, если аудио играет. </summary>
		public bool IsPlay { get; private set; }

		/// <summary> Получить имя трека. </summary>
		public string SongName { get; private set; }

		/// <summary> Возвращает true, если дорожка трека завершена. </summary>
		public bool TrackIsEnd
		{
			get
			{
				if (songStreamOffset == 0)
					return true;
				return (Bass.BASS_ChannelGetPosition(songStreamOffset) >=
				        Bass.BASS_ChannelGetLength(songStreamOffset));
			}
		}

		/// <summary> Температура громкости (медленно двигается к текущей громкости). </summary>
		public float Temperature { get; set; }

		/// <summary> Получить тип поведения темпа (уменьшается, увеличивается, в стандарт). </summary>
		public string GetTempoState
		{
			get { return tempoState.ToString(); }
		}

		/// <summary> Получить единицу текущего темпа. </summary>
		public float GetTempo
		{
			get { return tempo; }
		}

		/// <summary> Установить/получить громкость трека (от 0 до 100). </summary>
		public int Volume
		{
			set
			{
				volume = value;
				Bass.BASS_ChannelSetAttribute(songStreamOffset, BASSAttribute.BASS_ATTRIB_VOL, volume*0.01f);
			}
			get { return volume; }
		}

		/// <summary> Получить сумму всего FFT. </summary>
		public float GetSumSpectr
		{
			get { return sumSpectr; }
		}

		/// <summary> Получить сумму всего FFT (со сглаживанием). </summary>
		public float GetSmoothSumSpectr
		{
			get { return smoothSumSpectr; }
		}

		/// <summary> flac plugin is ready </summary>
		public bool FlacSupport
		{
			get { return pluginFlac != 0; }
		}

		#endregion

		#region Fields

		private TimeSpan timePos;
		private TimeSpan timeLen;
		private TimeSpan timeOffset;

		private bool useOffsetTime;

		private int fftAdded;
		private int index;

		/// <summary> Обработанный fft </summary>
		private float[] fltsCurrentValues;

		/// <summary> FFT оригинал-семпл </summary>
		private float[] fft;

		/// <summary> Сумма всех FFT значений </summary>
		private float sumSpectr;

		/// <summary> Сумма всех FFT значений (плавный) </summary>
		private float smoothSumSpectr;

		/// <summary> Главный поток трека </summary>
		private int songStreamOriginal;

		private int songStreamOriginalOffset;

		private int songStreamOffset;

		/// <summary> Громкость </summary>
		private int volume;

		/// <summary> Последняя ошибка </summary>
		private string lastError;

		private int frameCount;

		/// <summary> Время звучания трека </summary>
		private TimeSpan timeSong;

		private enum TempoState
		{
			ToNormal,
			ToDown,
			ToUp
		}

		/// <summary> Плавно увеличить темп (false - увеньшать) </summary>
		private TempoState tempoState;

		/// <summary> Дубликат потока трека, наложения эффектов, анализатор тпектра. </summary>
		private int songStreamFx;

		/// <summary> Темп трека </summary>
		private float tempo;

		private int pluginFlac;

		#endregion

		#region Methods

		/// <summary> Registers your BASS.NET version (for SpectrManager) and suppresses the freeware splash screen. </summary>
		/// <param name="eMail">Your eMail address for which you obtained a license. </param>
		/// <param name="registrationKey">The Registration-Key as obtained with the license.</param>
		/// <remarks> IMPORTANT: Make sure to call this method prior to any other BASS method! </remarks>
		public static void Registration(string eMail, string registrationKey)
		{
			BassNet.Registration(eMail, registrationKey);
		}

		/// <summary> Иницилизация BASS и загрузка имеющихся плагиов. </summary>
		/// <param name="device"> Устройство (-1) </param>
		/// <param name="freq"> Частота (44100) </param>
		/// <returns> Success </returns>
		public bool Init(int device = -1, int freq = 44100)
		{
			fltsCurrentValues = new float[129];
			fft = new float[2048];
			volume = 100;

			if (Bass.BASS_Init(device, freq, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero))
			{
				BassReady = true;
				base.Init(fltsCurrentValues.Length);

				// load flac plugin
				var flacPluginPath = Environment.CurrentDirectory + "\\bassflac.dll";
				if (File.Exists(flacPluginPath))
				{
					pluginFlac = Bass.BASS_PluginLoad(flacPluginPath);
				}

				lastError = "";
				return true;
			}

			lastError = "Не удалось иницилизировать BASS.";
			return false;
		}

		/// <summary> Загрузить трек (аудиофайл). </summary>
		/// <param name="pathSong"> Полный путь к треку. </param>
		/// <param name="songName"> Имя трека (если null, то задается автоматически). </param>
		/// <param name="offset"> Смещение позиции трека. </param>
		/// <param name="length"> Ограниченная длина трека... 0 = весь трек будет загружен. </param>
		/// <param name="andPlay"> Автоматически воспроизвести после загрузки (теряете фору). </param>
		/// <returns> Ошибка? </returns>
		public bool LoadSong(string pathSong, string songName = null, long offset = 0L, long length = 0L, bool andPlay = false)
		{
			if (songName == null)
			{
				songName = Path.GetFileNameWithoutExtension(pathSong);
			}

			timeSong = new TimeSpan(0, 0, 0, 0, 0);
			SongName = songName;
			base.ResetAll();
			
			// create
			songStreamOriginal = Bass.BASS_StreamCreateFile(pathSong, offset, length, BASSFlag.BASS_STREAM_DECODE);
			songStreamOriginalOffset = Bass.BASS_StreamCreateFile(pathSong, offset, length, BASSFlag.BASS_STREAM_DECODE);

			// spectr and fx
			songStreamFx = BassFx.BASS_FX_TempoCreate(songStreamOriginal, BASSFlag.BASS_FX_FREESOURCE);
			songStreamOffset = BassFx.BASS_FX_TempoCreate(songStreamOriginalOffset, BASSFlag.BASS_FX_FREESOURCE);

			Bass.BASS_ChannelSetAttribute(songStreamFx, BASSAttribute.BASS_ATTRIB_VOL, 0.0f);

			// получить сразу всю длину трека
			var trackLen = Bass.BASS_ChannelBytes2Seconds(songStreamOriginal, Bass.BASS_ChannelGetLength(songStreamOriginal, 0));
			timeLen = TimeSpan.FromSeconds(trackLen);

			// volume
			Volume = 100;

			// check error
			if (songStreamOriginal != 0 && Bass.BASS_ErrorGetCode() == BASSError.BASS_OK)
			{
				if (andPlay)
					Play();
				return true;
			}

			lastError = "Ошибка в загрузке трека.";
			return false;
		}

		/// <summary> Создать фору(отставание от спектра) для звука от спектра. Работает только до воспроизведение трека. </summary>
		/// <param name="secondsOffset"> Фора в секундах. </param>
		public bool CreateOffsetSong(double secondsOffset)
		{
			if (IsPlay || Bass.BASS_ChannelGetPosition(songStreamFx) > 0L)
				return true;

			useOffsetTime = true;
			timeOffset = TimeSpan.FromSeconds(secondsOffset);

			return false;
		}

		/// <summary> Изменить позицию дорожки. </summary>
		/// <param name="minutes"> Минут. </param>
		/// <param name="seconds"> Секунд </param>
		public void SetSongPos(double minutes, double seconds)
		{
			Bass.BASS_ChannelSetPosition(songStreamFx,
				Bass.BASS_ChannelSeconds2Bytes(songStreamFx, (minutes*60) + seconds),
				BASSMode.BASS_POS_BYTES);
		}

		/// <summary> Получить позицию музыки в процентах. </summary>
		/// <param name="prect"> Процент (максимальное значение100). </param>
		public float GetSongPosPerc(float prect)
		{
			if (songStreamOriginal == 0)
				return 0f;
			return
				((Bass.BASS_ChannelGetPosition(songStreamOriginal)*prect)/
				 Bass.BASS_ChannelGetLength(songStreamOriginal));
		}

		/// <summary> Получить позицию музыки в байтах. </summary>
		public long GetSongPos()
		{
			return Bass.BASS_ChannelGetPosition(songStreamOffset);
		}

		/// <summary> Выполняет все функции по обработке спектра, форы, темпа и т.д. Следует выполнять перед получением спектра трека. </summary>
		public void UpdateSpectrum()
		{
			if (songStreamFx == 0)
				return;

			if (TrackIsEnd && IsPlay)
			{
				if (Repeat)
				{
					Stop();
					Play();
				}
				else
				{
					// Stop
					IsPlay = false;
				}
			}

			if (!IsPlay || lastError.Length > 0)
				return;

			// фора
			if (useOffsetTime && timeOffset.Seconds + timeOffset.Milliseconds > 0)
			{
				var forOffsetTime =
					TimeSpan.FromSeconds(Bass.BASS_ChannelBytes2Seconds(songStreamFx,
						Bass.BASS_ChannelGetPosition(songStreamFx, 0)));

				if (timeOffset.Seconds <= forOffsetTime.Seconds
				    && timeOffset.Milliseconds <= forOffsetTime.Milliseconds)
				{
					timeOffset = TimeSpan.Zero;
					useOffsetTime = false;
					Bass.BASS_ChannelPlay(songStreamOffset, false);
				}
			}

			UpdateTempo();

			frameCount++;

			Bass.BASS_ChannelGetData(songStreamFx, fft, -0x7ffffffc); // -2147483644

			sumSpectr = 0f;
			fftAdded = 0;
			index = 0;
			fltsCurrentValues[0] = 0f;

			// TODO: math flts Current Values
			for (int j = 0; index < fltsCurrentValues.Length - 1 && j < 256; j++)
			{
				fltsCurrentValues[index] +=
					(float) Math.Sqrt(fft[j]*4)*SpectrConf.MultiForse;

				// add sum volume
				sumSpectr += fltsCurrentValues[index]/(SpectrConf.FFTInOne/1.5f);

				// combine
				if (++fftAdded == SpectrConf.FFTInOne)
				{
					fftAdded = 0;
					fltsCurrentValues[++index] = 0f;
				}
			}

			if (sumSpectr > 0f)
				sumSpectr /= SpectrConf.WeakeningSpectr;
			else
				sumSpectr = 0f;

			// сглаживание
			smoothSumSpectr += (sumSpectr - smoothSumSpectr)/SpectrConf.SmoothLevel;

			// температура
			Temperature += (sumSpectr - Temperature)/SpectrConf.TemperatureSpeed;

			base.UpdateParsing(fltsCurrentValues);
		}

		/// <summary> Обновить темп трека. </summary>
		private void UpdateTempo()
		{
			if (tempoState == TempoState.ToUp && tempo < SpectrConf.MaxTempo)
			{
				// up
				tempo += (SpectrConf.MaxTempo - tempo)/SpectrConf.TempSpeed + 0.1f;

				if (tempo >= SpectrConf.MaxTempo)
					tempo = SpectrConf.MaxTempo;
			}
			else if (tempoState == TempoState.ToDown && tempo > SpectrConf.MinTempo)
			{
				// down
				tempo += (SpectrConf.MinTempo - tempo)/SpectrConf.TempSpeed - 0.1f;

				if (tempo <= SpectrConf.MinTempo)
					tempo = SpectrConf.MinTempo;
			}
			else if (tempoState == TempoState.ToNormal)
			{
				// normal
				tempo += (-tempo)/SpectrConf.TempSpeed;

				if (tempo > -2f && tempo < 2f)
					tempo = 0f;
			}

			Bass.BASS_ChannelSetAttribute(songStreamFx, BASSAttribute.BASS_ATTRIB_TEMPO, (float) Math.Round(tempo, 1));
			Bass.BASS_ChannelSetAttribute(songStreamOffset, BASSAttribute.BASS_ATTRIB_TEMPO, (float) Math.Round(tempo, 1));
		}


		/// <summary> Воспроизвести. </summary>
		public void Play()
		{
			if (IsPlay || !BassReady || !SongReady)
				return;

			Bass.BASS_ChannelPlay(songStreamFx, false);
			if (!useOffsetTime)
				Bass.BASS_ChannelPlay(songStreamOffset, false);
			IsPlay = true;
		}

		/// <summary> Пауза. </summary>
		public void Pause()
		{
			if (!IsPlay)
				return;

			Bass.BASS_ChannelPause(songStreamFx);
			Bass.BASS_ChannelPause(songStreamOffset);
			IsPlay = false;
		}

		/// <summary> Стоп. </summary>
		public void Stop()
		{
			useOffsetTime = false;
			IsPlay = false;
			Temperature = 0f;

			Bass.BASS_ChannelStop(songStreamFx);
			Bass.BASS_ChannelSetPosition(songStreamFx, 0L);

			Bass.BASS_ChannelStop(songStreamOffset);
			Bass.BASS_ChannelSetPosition(songStreamOffset, 0L);

			for (int i = 0; i < fltsCurrentValues.Length; i++)
			{
				fltsCurrentValues[i] = 0f;
			}
		}

		/// <summary> Задать поведение темпа. </summary>
		/// <param name="flag"> 0 - в норму, 1 - поднять, 2 - уменьшить. </param>
		public void MoveTempoTo(byte flag)
		{
			switch (flag)
			{
				case 0:

					tempoState = TempoState.ToNormal;
					break;

				case 1:

					tempoState = TempoState.ToUp;
					break;

				case 2:

					tempoState = TempoState.ToDown;
					break;
			}
		}

		/// <summary> Выгрузить ВСЁ (аудио потоки, плагины, саму библиотеку bass). </summary>
		public void UnloadBass()
		{
			BassReady = false;
			UnloadSong();

			if (pluginFlac != 0)
			{
				Bass.BASS_PluginFree(pluginFlac);
				pluginFlac = 0;
			}

			Bass.BASS_Free();
		}

		/// <summary> Выгрузить аудио потоки. </summary>
		public void UnloadSong()
		{
			if (songStreamFx == 0)
				return;

			Stop();
			Bass.BASS_StreamFree(songStreamOffset);
			Bass.BASS_StreamFree(songStreamFx);
			Bass.BASS_StreamFree(songStreamOriginal);
			Bass.BASS_StreamFree(songStreamOriginalOffset);

			IsPlay = false;
			sumSpectr = 0f;

			for (int i = 0; i < fltsCurrentValues.Length; i = i + 1)
			{
				fltsCurrentValues[i] = 0f;
			}
		}

		#endregion
	}
}
