﻿using System;
using System.IO;
using System.Text;

namespace SpectrBoomBass
{
	/// <summary> Поля конфигураций </summary>
	public static class SpectrConf
	{
		/// <summary> Минимальная еденица темпа. </summary>
		public const float MaxTempo = 50f;

		/// <summary> Максимальная еденица темпа. </summary>
		public const float MinTempo = -80f;

		/// <summary> Скорость изменения темпа темпа. </summary>
		public const float TempSpeed = 32f;

		/// <summary> Кол. добавлений величины спектра за цикл, после открытия. </summary>
		public const int SpectrFrame = 6;

		/// <summary> Максимальный буфер памяти ноты спектра. </summary>
		public const int MaxSpectrBuffer = 64;


		/// <summary> Скорость, при которой высота ноты падает с суммированием скорости. </summary>
		public static float SpectrDownSpeed { get; set; }

		/// <summary> Слияние. Кол. Сумм величин спектра в одном обработанном массиве. </summary>
		public static int FFTInOne { get; set; }

		/// <summary> Движение температуры, величина деление к громкости. </summary>
		public static float TemperatureSpeed { get; set; }
		
		/// <summary> Множитель FFT[]. </summary>
		public static float MultiForse { get; set; }

		/// <summary> Сглаживание всей суммы спектра (деление). </summary>
		public static float WeakeningSpectr { get; set; }

		/// <summary> Сглаживание одной суммы спектра. </summary>
		public static float SmoothLevel { get; set; }
		
		/// <summary> Пропуск нот спектра, рядом с наивысшим (в обе стороны). </summary>
		public static int HightSpectrsStep { get; set; }
		
		/// <summary> Количество высоких нот спектра. </summary>
		public static int HightSpectrsCount { get; set; }
		
		/// <summary> Деление самой высокой ноты как определение для самой низкой. </summary>
		public static float NextMaxValueHightNote { get; set; }

		/// <summary> Минимальная высота для определение высокой ноты. </summary>
		public static float MinValueHightNote { get; set; }

		static SpectrConf()
		{
			Reset();
		}

		/// <summary> Reset all to default. </summary>
		public static void Reset()
		{
			NextMaxValueHightNote = 2.5f;
			MinValueHightNote = 32f;
			TemperatureSpeed = 128f;
			SmoothLevel = 2.5f;
			MultiForse = 60f;
			WeakeningSpectr = 8f;
			HightSpectrsStep = 10;
			HightSpectrsCount = 6;
			FFTInOne = 2;
			SpectrDownSpeed = 1.35f;
		}


		private static int textLine;

		/// <summary> Загрузить настройки из файла. Возвращает true, если поля изменились. </summary>
		/// <param name="fullPath"> Полный путь к файлу. </param>
		public static bool LoadConfigFile(string fullPath)
		{
			if (File.Exists(fullPath))
			{
				textLine = 0;
				try
				{
					LoadConf(new StreamReader(fullPath, Encoding.UTF8));
					return true;
				}
				catch (Exception exc)
				{
					using (var ct = File.CreateText(Environment.CurrentDirectory + "\\LoadConf_Error.txt"))
					{
						ct.WriteLine("Line: " + textLine + "\n\r" + exc.Message);
					}
				}
			}

			return false;
		}

		private static void LoadConf(StreamReader sr)
		{
			while (!sr.EndOfStream)
			{
				string t = sr.ReadLine();
				textLine++;

				if (t == null || t.Trim().Length == 0 || !t.Contains("="))
					continue;

				// get value
				float val = Convert.ToSingle(
					t.Remove(0, t.IndexOf('=') + 1).
						Trim().Replace('.', ','));

				// get name
				string name = t.Remove(t.IndexOf('=') - 1).Trim();

				switch (name)
				{
					case "NextMaxValueHightNote":
						NextMaxValueHightNote = val;
						break;

					case "MinValueHightNote":
						MinValueHightNote = val;
						break;

					case "TemperatureSpeed":
						TemperatureSpeed = val;
						break;

					case "SmoothLevel":
						SmoothLevel = val;
						break;

					case "MultiForse":
						MultiForse = val;
						break;

					case "WeakeningSpectr":
						WeakeningSpectr = val;
						break;

					case "HightSpectrsStep":
						HightSpectrsStep = (int) val;
						break;

					case "HightSpectrsCount":
						HightSpectrsCount = (int) val;
						break;

					case "FFTInOne":
						FFTInOne = (int) val;
						break;

					case "SpectrDownSpeed":
						SpectrDownSpeed = val;
						break;
				}

			}
		}

	}
}
