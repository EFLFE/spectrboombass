﻿using System;
using System.Collections.Generic;

namespace SpectrBoomBass
{
	/// <summary> SpectrParsing Manager for SpectrManager </summary>
	public class SpectrParsingManager
	{
		/// <summary> Total Jump Notes </summary>
		public int TotalJumpNotes { get; private set; }

		/// <summary> Высота самой высокой ноты. </summary>
		public float MaxHightValue
		{
			get { return superMax; }
		}
		
		/// <summary> Высота самой низкой ноты из высоких. </summary>
		public float MinHightValue
		{
			get { return superMin; }
		}

		/// <summary> Получить стек спектра, деленный на SpectrFrame и сумирован в каждом. 
		/// Ниже список по умолчанию (6): 
		/// <remarks> [0] - Bass.
		/// <remarks> [1..n] - next... </remarks> </remarks> </summary>
		public float[] GetFFTStack
		{
			get { return fftStack; }
		}

		private float[] fftStack;
		private List<SpectrParsing> spectrParsing;
		private int steps;
		private float superMax;
		private float superMin;

		/// <summary> Init </summary>
		/// <param name="fltsLength"> fltsCurrentValues.Length </param>
		protected void Init(int fltsLength)
		{
			fftStack = new float[SpectrConf.SpectrFrame]; // safe

			spectrParsing = new List<SpectrParsing>();
			for (int i = 0; i < fltsLength; i++)
			{
				spectrParsing.Add(new SpectrParsing(i));
			}
		}

		/// <summary> Update SpectrParsing </summary>
		/// <param name="flts"> fltsCurrentValues </param>
		protected void UpdateParsing(float[] flts)
		{
			fftStack = new float[SpectrConf.SpectrFrame];
			int countInStack = (int) Math.Round((double) flts.Length/SpectrConf.SpectrFrame, 0);
			int countAdded = 0;
			int stackIndex = 0;

			TotalJumpNotes = 0;
			for (int i = 0; i < spectrParsing.Count && i < flts.Length; i++)
			{
				spectrParsing[i].Update(flts[i]);

				// + jump count
				if (spectrParsing[i].IsJump)
					TotalJumpNotes++;

				// add in stack array
				if (countAdded++ == countInStack)
				{
					countAdded = 0;
					stackIndex++;
				}
				fftStack[stackIndex] += spectrParsing[i].GetValue;

			}

			FindHight();
		}

		/// <summary> Reset SpectrParsing list </summary>
		protected void ResetAll()
		{
			spectrParsing.ForEach(s => s.Reset());
		}

		private void FindHight()
		{
			int i;
			// Поиск наивысшых нот (велосипедный алгоритм!)
			// Первая нота (=superMax) самая высокая,
			// следующие высокие ноды не должны быть меньше самой высокой/nextMax

			steps = 0;
			superMax = 1f;
			superMin = float.MaxValue;

			while (steps < SpectrConf.HightSpectrsCount)
			{
				// поиск самой высокой ноты

				int index = -1;
				float max = 0f;

				for (i = 0; i < spectrParsing.Count; i++)
				{
					if (spectrParsing[i].LockCheck)
						continue;

					if (steps != 0)
					{
						if (max < spectrParsing[i].GetValue
						    && spectrParsing[i].GetValue > SpectrConf.MinValueHightNote
						    && spectrParsing[i].GetValue >= superMax/SpectrConf.NextMaxValueHightNote)
						{
							index = i;
							max = spectrParsing[i].GetValue;
						}
					}
					else if (max < spectrParsing[i].GetValue
					         && spectrParsing[i].GetValue > SpectrConf.MinValueHightNote)
					{
						// FIRST
						index = i;
						max = spectrParsing[i].GetValue;
						superMax = max;
					}
				}

				steps++;
				if (index == -1)
					continue; // не найден

				// применение высокой ноты

				if (spectrParsing[index].HeightStatus < 2)
					spectrParsing[index].HeightStatus++;

				// SET HEIGHT
				spectrParsing[index].IsHigh = true;

				// самый низкий из высоких
				superMin = Math.Min(superMin, spectrParsing[index].GetValue);

				// lock check
				for (i = index; index - SpectrConf.HightSpectrsStep <= i && i >= 0; i--)
				{
					spectrParsing[i].LockCheck = true;
				}

				for (i = index + 1; index + SpectrConf.HightSpectrsStep >= i && i < spectrParsing.Count; i++)
				{
					spectrParsing[i].LockCheck = true;
				}

			}

			for (i = 0; i < spectrParsing.Count; i++)
			{
				if (!spectrParsing[i].IsHigh)
					spectrParsing[i].HeightStatus = 0;
			}

		}


		/// <summary> Получить интерфейс из массива ISpectrParsing. </summary>
		/// <param name="index"> Индекс. </param>
		[System.Runtime.CompilerServices.IndexerName("GetISpectrParsingItem")]
		public ISpectrParsing this[int index]
		{
			get { return spectrParsing[index]; }
		}

		/// <summary> Получить массив всех ISpectrParsing для полной информации о спектре. </summary>
		public ISpectrParsing[] GetSpectrParsingData
		{
			get
			{
				var isp = new ISpectrParsing[spectrParsing.Count];
				for (int i = 0; i < isp.Length; i++)
					isp[i] = spectrParsing[i];
				return isp;
			}
		}

	}
}
