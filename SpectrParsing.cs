﻿using System.Collections.Generic;

namespace SpectrBoomBass
{
	/// <summary> Единица спектра. Общий анализ на события трека. </summary>
	public class SpectrParsing : ISpectrParsing
	{
		// Fields
		private float posValue; // высота
		private float downSpeed; // скорость падения
		private bool jump; // подмимается
		private bool isHigh; // является одним из высоких
		private byte jumpPower; // сила для прыжка (highJump) или сколько кадров он пропустит
		private byte jumpWait; // время, когда jumpPower может быть равен true *если не успевает стать высоким)
		private bool highJump; // был высокий прыжок (высота больше в два раза, чем до n раньше)

		// buffer
		private readonly List<float> bufferValue;

		public float[] GetBufferValue
		{
			get { return bufferValue.ToArray(); }
		}

		public int GetBufferLength
		{
			get { return bufferValue.Count; }
		}

		/// <summary> Был высокий прыжок. </summary>
		public bool WasHighJump { get { return highJump && HeightStatus > 0; } }

		/// <summary> Статус высокой ноты:
		/// <remarks> 0 - Не высокий.
		/// <remarks> 1 - Стал высоким.
		/// <remarks> 2 - Остаётся высоким. </remarks></remarks></remarks> </summary>
		public byte HeightStatus { get; set; }

		/// <summary> Зактыр для проверки на высокую ноту. [HightSpectrsStep] </summary>
		public bool LockCheck { get; set; }

		/// <summary> Высота данной единицы спектра. </summary>
		public float GetValue
		{
			get { return posValue; }
		}

		/// <summary> Поднимается ли спектр. </summary>
		public bool IsJump
		{
			get { return jump; }
		}

		/// <summary> Является наивысшей (или одной из) нотой спектра. </summary>
		public bool IsHigh
		{
			get { return isHigh; }
			set { isHigh = value; }
		}

		public int GetIndex
		{
			get { return index; }
		}

		private readonly int index;



		public SpectrParsing(int index)
		{
			this.index = index;
			bufferValue = new List<float>(SpectrConf.MaxSpectrBuffer + 1);
		}

		/// <summary> Сброс. </summary>
		public void Reset()
		{
			posValue = 0f;
			jump = false;
			downSpeed = 0;
			bufferValue.Clear();
		}

		/// <summary> Обновить спектр. </summary>
		/// <param name="valueFft">Текущеая величина спектра для данной единицы. </param>
		public void Update(float valueFft)
		{
			if (valueFft < 0f)
				valueFft = 0f;

			if (valueFft > posValue)
			{
				posValue = valueFft;
				jump = true;
				downSpeed = 0f;
			}
			else // down
			{
				downSpeed += SpectrConf.SpectrDownSpeed;
				posValue -= downSpeed;
				jump = false;
			}

			if (posValue < 0f)
				posValue = 0;
			
			// reset
			LockCheck = false;
			isHigh = false;

			// add buffer

			bufferValue.Insert(0, posValue);

			if (bufferValue.Count >= SpectrConf.MaxSpectrBuffer)
				bufferValue.RemoveAt(bufferValue.Count - 1);

			// High jump ? offset +3

			if (jumpWait > 0)
			{
				jumpWait--;
			}
			else
			{
				highJump = false;
			}

			if (jumpPower > 0)
			{
				jumpPower--;
				return;
			}

			const float m = 1.7f;

			if (bufferValue.Count > 4
			    && (bufferValue[1]*m < posValue
			        || bufferValue[2]*m < posValue
			        || bufferValue[3]*m < posValue
			        || bufferValue[4]*m < posValue))
			{
				highJump = true;
				jumpPower = 6;
				jumpWait = 3;
			}

		}

	}
}
